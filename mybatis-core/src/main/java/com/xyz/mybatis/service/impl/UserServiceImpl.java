package com.xyz.mybatis.service.impl;

import com.xyz.mybatis.dao.UserMapper;
import com.xyz.mybatis.pojo.User;
import com.xyz.mybatis.service.IUserService;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Project Name:mybatis <br/>
 * Package Name:com.xyz.mybatis.service.impl <br/>
 * Date:2019/4/24 11:51 <br/>
 * <b>Description:</b> TODO: 描述该类的作用 <br/>
 *
 * @author <a href="mailto:xuyz@eastcom-sw.com">Xuyz</a><br/>
 * Copyright Notice =========================================================
 * This file contains proprietary information of Eastcom Technologies Co. Ltd.
 * Copying or reproduction without prior written approval is prohibited.
 * Copyright (c) 2019 =======================================================
 */

@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements IUserService {

    @Resource
    private UserMapper userDao;

    @Override
    public List<User> getUser() {
        return userDao.selectAll();
    }

    @Transactional
    @Override
    public int addUser(User user) {
        return userDao.insert(user);
    }
}

package com.xyz.mybatis.service;

import com.xyz.mybatis.pojo.User;
import java.util.List;

/**
 * Project Name:mybatis <br/>
 * Package Name:com.xyz.mybatis.service <br/>
 * Date:2019/4/24 11:51 <br/>
 * <b>Description:</b> TODO: 描述该类的作用 <br/>
 *
 * @author <a href="mailto:xuyz@eastcom-sw.com">Xuyz</a><br/>
 * Copyright Notice =========================================================
 * This file contains proprietary information of Eastcom Technologies Co. Ltd.
 * Copying or reproduction without prior written approval is prohibited.
 * Copyright (c) 2019 =======================================================
 */
public interface IUserService {

    List<User> getUser();
    int addUser(User user);
}

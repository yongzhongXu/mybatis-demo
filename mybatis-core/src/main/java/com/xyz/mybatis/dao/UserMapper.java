package com.xyz.mybatis.dao;

import com.xyz.mybatis.pojo.User;
import java.util.List;
import org.springframework.stereotype.Component;

public interface UserMapper {
    int deleteByPrimaryKey(String id);

    int insert(User record);

    User selectByPrimaryKey(String id);

    List<User> selectAll();

    int updateByPrimaryKey(User record);
}

package com.xyz.mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = "com.xyz.mybatis")
@MapperScan("com.xyz.mybatis.dao")
public class MybatisWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisWebApplication.class, args);
    }

}

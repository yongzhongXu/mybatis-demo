package com.xyz.mybatis.controller;

import com.xyz.mybatis.exception.MyException;
import com.xyz.mybatis.pojo.User;
import com.xyz.mybatis.service.IUserService;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Project Name:mybatis <br/>
 * Package Name:com.xyz.mybatis.controller <br/>
 * Date:2019/4/24 11:47 <br/>
 * <b>Description:</b> TODO: 描述该类的作用 <br/>
 *
 * @author <a href="mailto:xuyz@eastcom-sw.com">Xuyz</a><br/>
 * Copyright Notice =========================================================
 * This file contains proprietary information of Eastcom Technologies Co. Ltd.
 * Copying or reproduction without prior written approval is prohibited.
 * Copyright (c) 2019 =======================================================
 */

@Controller
@RequestMapping("user")
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final ExecutorService executor = Executors.newFixedThreadPool(3);

    @Resource
    private IUserService userService;

    @ResponseBody
    @RequestMapping("/getUser")
    public List<User> getUser() {
        logger.info("日记=====================");
        executor.execute(new Runnable() {
            @Override
            public void run() {

            }
        });
        return userService.getUser();
    }

    @RequestMapping("/addUser")
    public String addUser(Model model) {
        logger.info("注册用户=====================");
        User user = new User();
        user.setName("xuyz");
        user.setPassword("123456");
        try {
           int result = userService.addUser(user);
            if(result==1){
                model.addAttribute("addName",user.getName()+"用户添加成功，请登录。。。");
                return "login";
            }
        } catch (Exception e) {
            logger.info("添加用户失败");
            throw new MyException("添加用户失败:"+e.getMessage());
        }
        return null;
    }
}

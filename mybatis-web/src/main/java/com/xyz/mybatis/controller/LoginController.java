package com.xyz.mybatis.controller;

import com.xyz.mybatis.config.LoginWebConfig;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Project Name:mybatis <br/>
 * Package Name:com.xyz.mybatis.controller <br/>
 * Date:2019/6/25 14:36 <br/>
 * <b>Description:</b> TODO: 描述该类的作用 <br/>
 *
 * @author <a href="mailto:xuyz@eastcom-sw.com">Xuyz</a><br/>
 * Copyright Notice =========================================================
 * This file contains proprietary information of Eastcom Technologies Co. Ltd.
 * Copying or reproduction without prior written approval is prohibited.
 * Copyright (c) 2019 =======================================================
 */
@Controller
@RequestMapping("login")
public class LoginController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/loginPage")
    public String login(){
        return "login";
    }

    @RequestMapping(value="/loginAction", method=RequestMethod.POST)
    public String loginAction(@RequestParam("userName") String userName,@RequestParam("password") String password,Model model ,HttpServletRequest request){
        logger.info("userName:"+userName+"===password:"+password);
        if("admin".equals(userName) && "admin".equals(password)){
            request.getSession().setAttribute(LoginWebConfig.SESSION_KEY,userName);
            model.addAttribute(LoginWebConfig.SESSION_KEY,userName);
            return "user";
        }
        model.addAttribute("message","用户名，密码不匹配，请重新输入");
        return "login";
    }


    @RequestMapping(value = "/logoutAction", method = RequestMethod.GET)
    public String logoutAction( Model model, HttpServletRequest request) {

        request.getSession().removeAttribute(LoginWebConfig.SESSION_KEY);
        return "login";
    }
}

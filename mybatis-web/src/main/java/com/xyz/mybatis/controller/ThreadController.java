package com.xyz.mybatis.controller;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Project Name:mybatis <br/>
 * 实现多线程的几种方式
 * 1.继承Thread类
 * 2.实现runnable接口
 * 3.Executor框架 ,使用线程池机制,提供ExecutorService接口，Callable接口，Future接口
 *   Executors类，提供了一系列工厂方法用于创建线程池，返回的线程池都实现了ExecutorService接口。
 *   public static ExecutorService newFixedThreadPool(int nThreads)
 */
public class ThreadController {

    private static final ExecutorService executor = Executors.newFixedThreadPool(3);

   /*
   1、public static ExecutorService newFiexedThreadPool(int Threads) 创建固定数目线程的线程池。
   2、public static ExecutorService newCachedThreadPool()：创建一个可缓存的线程池，调用execute 将重用以前构造的线程（如果线程可用）。如果没有可用的线程，则创建一个新线程并添加到池中。终止并从缓存中移除那些已有 60 秒钟未被使用的线程。
   3、public static ExecutorService newSingleThreadExecutor()：创建一个单线程化的Executor。
   4、public static ScheduledExecutorService newScheduledThreadPool(int corePoolSize) 创建一个支持定时及周期性的任务执行的线程池，多数情况下可用来替代Timer类。
   */


    public void threadTest(){

        //继承Thread类==================
        new Thread(){
            public void run() {
                System.out.println("Thread============");
            }
        }.start();

        //实现runnable接口==================
        new Thread(new Runnable(){
            @Override
            public void run() {
                System.out.println("Runnable============");
            }
        }).start();


        //Executor框架 Executor接口execute方法==================
       executor.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("executor============");
            }
        });

        //Executor框架 ExecutorService接口submit方法==================
        Future future=executor.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("executor============");
            }
        });

        System.out.println("==========future:"+future);
//        executor.shutdown();

    }
}

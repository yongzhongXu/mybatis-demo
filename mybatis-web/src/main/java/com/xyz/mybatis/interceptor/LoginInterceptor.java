package com.xyz.mybatis.interceptor;

import com.xyz.mybatis.config.LoginWebConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * Project Name:mybatis <br/>
 * Package Name:com.xyz.mybatis.interceptor <br/>
 * Date:2019/5/13 17:58 <br/>
 * <b>Description:</b> TODO: 描述该类的作用 <br/>
 *
 * @author <a href="mailto:xuyz@eastcom-sw.com">Xuyz</a><br/>
 * Copyright Notice =========================================================
 * This file contains proprietary information of Eastcom Technologies Co. Ltd.
 * Copying or reproduction without prior written approval is prohibited.
 * Copyright (c) 2019 =======================================================
 */
public class LoginInterceptor extends HandlerInterceptorAdapter {
    //目标方法执行之前
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Object user = request.getSession().getAttribute(LoginWebConfig.SESSION_KEY);

        if(user == null){
            //未登陆，返回登陆页面
            request.setAttribute("msg","没有权限请先登陆");
            response.sendRedirect("/mybatis/login/loginPage");
            return false;
        }else{
            //已登陆，放行请求
            return true;
        }
    }
}

====================================
jsp根据数据设定默认显示：<select id="auditFlag" name="auditFlag" class="form-control"
											style="width:230px;">
										<option value = "0" ${docCatalog.auditFlag=="0"?'selected':''}>否</option>
										<option value = "1" ${docCatalog.auditFlag=="1"?'selected':''}>是</option>
									</select>

=================================
 前端获取时间，可以val 直接显示在时间组件
 getlastMonthTime:function () {
        // var year=2019;
        // var month=3;
        // var day=30
        // var myDate=new Date(year,month-1,day);
var value=myDate.format("yyyy-MM-dd");
        var myDate=new Date();
        // alert(myDate.getFullYear() + "-" + (myDate.getMonth()+1) + "-" + myDate.getDate());
        myDate.setMonth(myDate.getMonth()-1);
        // alert(myDate.getFullYear() + "-" + (myDate.getMonth()+1) + "-" + myDate.getDate());
        return myDate.getFullYear() + "-" + (myDate.getMonth()+1) + "-" + myDate.getDate();
    }
======================================
对象转字符串：JSON.stringify(Object)  // JSONArray.toJSONString()
字符串转对象：JSONArray data = JSONArray.parseArray(strList);
             JSONObject jobj = (JSONObject) data.get(i);
====================================
创建实体注解：
@Entity
@Table(name = "act_biz_info_online", catalog = "esflow")
@DynamicUpdate
@DynamicInsert
设置默认值：
@Column(nullable = false, name = "COMPLAIN_COUNT",columnDefinition="Integer default 0" )
@Column(nullable = false, name = "COMPLAIN_COUNT",columnDefinition="userName" )
======================================
spring :
Atomic原子性：事务是由一个或多个活动所组成的一个工作单元。原子性确保事务中的所有操作全部发生或者全部不发生。
Consistent一致性：一旦事务完成，系统必须确保它所建模的业务处于一致的状态
Isolated隔离线：事务允许多个用户对象头的数据进行操作，每个用户的操作不会与其他用户纠缠在一起。
Durable持久性：一旦事务完成，事务的结果应该持久化，这样就能从任何的系统崩溃中恢复过来。
===================================

springboot 默认集成hibernate：
      只要集成mybatis或者jpa就行,启动类扫描dao包 @MapperScan("com.winter.mapper")，到包添加@Mapper注解，才能被扫描到
      1.pom文件添加mysql-connector-java，mybatis-generator-core依赖，2.generator插件
      2.在resources下创建mybatis-generator.xml（配置数据库信息，生成Model类存放位置，生成映射文件存放位置，生成Dao类存放位置，根据对应的表（可以多个）生成对应类型）
      3、在src/main/java/com/demo/下创建dao、model、mapper文件夹。
      4、在maven project下双击mybatis-generator插件下的mybatis-generator:generate
=================================
spring MVC框架主要解决WEB开发，网络接口
spring boot使用了默认(约定)大于配置的理念,能快速开发单个微服务；
spring cloud大部分的功能插件都是基于springBoot去实现的，springCloud关注于全局的微服务整合和管理，将多个springBoot单体微服务进行整合以及管理；        springCloud依赖于springBoot开发，而springBoot可以独立开发；

事务就是对一系列的数据库操作（比如插入多条数据）进行统一的提交或回滚操作，如果插入成功，那么一起成功，如果中间有一条出现异常，那么回滚之前的所有操作。这样可以防止出现脏数据，防止数据库数据出现问题。开发中为了避免这种情况一般都会进行事务管理。

===============================
get resfush请求：

http://localhost:9530/myouth/product/1
@GetMapping("/product/{id}")
    public String getProduct(@PathVariable String id) {}

http://localhost:9530/myouth/test2?id=12
@GetMapping("/test2")
    public String test2( @RequestParam String id) {}

======================================

springboot 2.0
org.springframework.boot.bind.RelaxedDataBinder
错误原因： org.springframework.boot.bind 包已经删掉了,导致RelaxedPropertyResolver这个方法已经不可用了.去掉alibaba 中druid-spring-boot-starter依赖

 <!--<dependency>-->
            <!--<groupId>com.alibaba</groupId>-->
            <!--<artifactId>druid-spring-boot-starter</artifactId>-->
            <!--<version>1.1.0</version>-->
        <!--</dependency>-->
